﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Ads : MonoBehaviour
{
    private void Start()
    {
        Advertisement.Initialize("3679653", true);
    }
    public void ShowAd()
    {
         if (Advertisement.IsReady())
         {
             Advertisement.Show();
         }
    }
}
