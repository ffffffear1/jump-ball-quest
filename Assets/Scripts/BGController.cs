﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGController : MonoBehaviour
{
    public Sprite[] bg;
    void Start()
    {
        GetComponent<Image>().sprite = bg[Random.Range(0, bg.Length)];
    }
}
