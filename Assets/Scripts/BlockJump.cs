﻿using System;
using UnityEngine;
public class BlockJump : MonoBehaviour
{
    public Rigidbody2D rb;
    public new Collider2D collider;
    bool isJumping = false;
    bool isJumpingArrow = false;
    private float speedArrow = 3.8f;
    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (isJumpingArrow == true)
        {
            //rb.AddForce(new Vector3(0, 1 * speedArrow * Time.deltaTime), 0);
            collider.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 1 * speedArrow * Time.deltaTime), 0);
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("touch");
        if (gameObject.CompareTag("Black"))
        {
            isJumping = true;
        }
        if (gameObject.CompareTag("Blue"))
        {
            isJumping = true;
            Destroy(this.gameObject);
        }
        if (gameObject.CompareTag("Arrow"))
        {
            //print(collision.gameObject, collider.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 1 * speedArrow * Time.deltaTime), 0));
            isJumpingArrow = true;
            //Destroy(this.gameObject);
        }
    }
}