﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BonusController : MonoBehaviour
{
    [HideInInspector]public int bonusScore;            //Значение бонусов//
    public int BonusInPop;
    public TextMeshProUGUI BonusText;
    public int Bonus;

    public void AmountBonus()
    {
        BonusInPop = bonusScore * 2 - 1;
        Bonus += PlayerPrefs.GetInt("Money") + BonusInPop;
        PlayerPrefs.SetInt("Money", Bonus);
    }
}
