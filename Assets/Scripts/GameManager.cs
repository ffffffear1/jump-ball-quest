﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager GMSingleton;
    public float coin;
    public bool vibration = true;
    public bool music = true;



    public int indexlevel;

    public void Awake()
    {
        if (!GMSingleton)
        {
            DontDestroyOnLoad(gameObject);
            GMSingleton = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void Vibration()
    {
        if (vibration == true)
        {
            Handheld.Vibrate();
        }
    }

    public void Music()
    {
        if (music == true)
        {
            //Debug.Log("++");
        }
    }
}
