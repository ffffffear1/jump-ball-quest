﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour
{
    public GameObject particle;
    
    public  Rigidbody2D rb;
    public bool Jump;
    public float _jump;
    public float _move;
    BonusController bonusController;

    public bool CanMove;// = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        GameObject Finish = GameObject.Find("Player");          //Тут он получает доступ к переменной в BonusController//
        bonusController = Finish.GetComponent<BonusController>();
       // CanMove = false;
    }

    private void FixedUpdate()
    {
        if (Jump)
        {
            rb.velocity = new Vector2(0, _jump * Time.deltaTime);
            GameManager.GMSingleton.Vibration();
            SoundManаger.SMSingleton.JumpMusic();
        }
        if (CanMove == true)
        {
            if (Input.GetKey(KeyCode.D ) /*&& CanMove == true*/)
            {
                transform.Translate(Vector3.right * _move * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A) /*&& CanMove == true*/)
            {
                transform.Translate(Vector3.left * _move * Time.deltaTime);
            }
        }
        _jump = 120f;
        Jump = false;
        Debug.Log(PlayerPrefs.GetInt("Money"));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Black")
        {
            Jump = true;
            if(collision.gameObject.GetComponent<Animator>() != null)
                collision.gameObject.GetComponent<Animator>().SetTrigger("JumpAnim");
        }
        if(collision.gameObject.tag == "Arrow")
        {
            rb.velocity = new Vector2(0, 200f * Time.deltaTime);
            GameManager.GMSingleton.Vibration();
            SoundManаger.SMSingleton.JumpMusic();
        }
        if(collision.gameObject.tag == "Blue")
        {
            Jump = true;            
            
            if(collision.gameObject.GetComponent<Animator>() != null) 
                collision.gameObject.GetComponent<Animator>().SetTrigger("JumpAnim2");
            
            Destroy(collision.gameObject, 0.2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bonus")
        {
            var bonusParticle= Instantiate(particle, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            Debug.Log("Bonus");
            GameManager.GMSingleton.Vibration();
            SoundManаger.SMSingleton.BonusMusic();
            bonusController.bonusScore++;                 //Тут он передает значение в BonusController
            Destroy(bonusParticle, 1);
            Destroy(collision.gameObject);
        }
        if(collision.gameObject.tag == "Obsticle")
        {
            SceneController._SceneController.Dead.SetActive(true);
            GameManager.GMSingleton.Vibration();
            SoundManаger.SMSingleton.DeadMusic();
            Destroy(gameObject);
        }
        if(collision.gameObject.tag == "finish")
        {
            bonusController.AmountBonus();
            GameManager.GMSingleton.Vibration();
            SoundManаger.SMSingleton.WinMusic();
            bonusController.BonusText.text = bonusController.BonusInPop.ToString();
            SceneController._SceneController.Finish.SetActive(true);
            Destroy(gameObject);
        }
    }
}

