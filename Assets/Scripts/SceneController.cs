﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public static SceneController _SceneController;

    public GameObject _startPanel;

    public GameObject _pause;
    public GameObject Dead;
    public GameObject Finish;
    public GameObject _popOnExit;
    public GameObject iconPause;

    public Ads ads;

    public Button _accept;
    public Button _deny;

    private Move player;

    public Button Vibration;
    public Sprite VibrationOn;
    public Sprite VibrationOff;
    public Button Music;
    public Sprite MusicOn;
    public Sprite MusicOff;
    
    

    public Slider SliderBg;
    public Slider SliderEffect;

    private void Start()
    {
        player = FindObjectOfType<Move>();
        _accept.onClick.AddListener(Accept);
        _deny.onClick.AddListener(Deny);
        ads = GameObject.Find("Player").GetComponent<Ads>();

        if(GameManager.GMSingleton.vibration == false)
        {            
            Vibration.image.sprite = VibrationOff;
        }
        else
        {
            Vibration.image.sprite = VibrationOn;
        }
        if (GameManager.GMSingleton.music == false)
        {            
            Music.image.sprite = MusicOff;
        }
        else
        {            
            Music.image.sprite = MusicOn;
        }
        SoundManаger.SMSingleton.BGNew();
    }

    private void Awake()
    {
        if (_SceneController == null)
        {
            _SceneController = this;
        }
        else if (_SceneController == this)
        {
            Destroy(this.gameObject);
        }
        SoundManаger.SMSingleton.NewSlider(SliderBg, SliderEffect);
    }

    public void PopOn()
    {
        _popOnExit.SetActive(true);
    }

    public void StartGame()
    {
        _startPanel.SetActive(false);
        player.rb.bodyType = RigidbodyType2D.Dynamic;
        player.CanMove = true;
    }

    public void SceneLoader(int scene)
    {
        ads.ShowAd();
        SceneManager.LoadScene(scene);
    }

    public void Pause(bool pause)
    {
        if (pause == false)
        {
            iconPause.SetActive(false);
            _pause.SetActive(true);
            player.rb.bodyType = RigidbodyType2D.Static;
            player.CanMove = false;
        }
        else if (pause == true)
        {
            iconPause.SetActive(true);
            _pause.SetActive(false);
            player.rb.bodyType = RigidbodyType2D.Dynamic;
            player.CanMove = true;
        }
    }

    public void Restart()
    {
        // SceneManager.LoadScene(GameManager.GMSingleton.indexlevel);
        ads.ShowAd();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Deny()
    {
        _popOnExit.SetActive(false);
    }

    public void Accept()
    {
        SceneManager.LoadScene(0);
    }

    public void VibrationOnOff()
    {
        if (GameManager.GMSingleton.vibration == true)
        {
            Vibration.image.sprite = VibrationOff;
            GameManager.GMSingleton.vibration = false;
        }
        else
        {
            Vibration.image.sprite = VibrationOn;
            GameManager.GMSingleton.vibration = true;
        }
    }
    public void MusicOnOff()
    {
        if (GameManager.GMSingleton.music == true)
        {
            Music.image.sprite = MusicOff;
            GameManager.GMSingleton.music = false;
        }
        else
        {
            Music.image.sprite = MusicOn;
            GameManager.GMSingleton.music = true;
        }
        SoundManаger.SMSingleton.BGNew();
    }


private bool On = false;
public void Option(GameObject Panel)
{

if (On == true)
{
    Panel.SetActive(false);
    On = false;
}
else
{
    Panel.SetActive(true);
    On = true;
}

}

}