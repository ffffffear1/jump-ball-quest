﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SkinChanger : MonoBehaviour
{
    public GameObject player;

    public Sprite[] spriteImages;


    public TextMeshProUGUI skinText1;
    public TextMeshProUGUI skinText2;
    public TextMeshProUGUI money;

    public SpriteSerializable dict;

    public SpriteSettings spriteScriptableObjects;

    private void Start()
    {
        spriteImages = Resources.LoadAll<Sprite>("Sprites");
    }
    private void Update()
    {
        money.text = "Монет : " + PlayerPrefs.GetInt("Money");
        for (int i = 0; i < spriteImages.Length; i++)
        {
            if (PlayerPrefs.GetString("skin") == spriteImages[0].ToString())
            {
                skinText1.gameObject.SetActive(true);
                skinText2.gameObject.SetActive(false);
            }
            if (PlayerPrefs.GetString("skin") == spriteImages[1].ToString())
            {
                skinText2.gameObject.SetActive(true);
                skinText1.gameObject.SetActive(false);
            }
        }
    }
    public void SkinChanged(GameObject buy, int coast, int index)
    {
        if (PlayerPrefs.GetInt("Money") >= coast && !dict.ContainsKey(spriteImages[index]))
        {
            player.GetComponent<SpriteRenderer>().sprite = spriteImages[index];
            int currentMoney = PlayerPrefs.GetInt("Money");
            PlayerPrefs.SetInt("Money", currentMoney - coast);
            Debug.Log("спрайта нету");
            dict.Add(spriteImages[index], true);
        }
        if (dict.ContainsKey(spriteImages[index]))
        {
            player.GetComponent<SpriteRenderer>().sprite = spriteImages[index];
            Debug.Log("спрайт есть");
        }
    }

    public void Skin1()
    {
        SkinChanged(this.gameObject, 10, 0);
    }

    public void Skin2()
    {
        SkinChanged(this.gameObject, 20, 1);
    }

    public void SaveSkin()
    {
        //spriteScriptableObjects.sprite.Add(player.GetComponent<SpriteRenderer>().sprite);
    }

    public void CheckSkin()
    {
        Debug.Log(player.GetComponent<SpriteRenderer>().sprite);
    }

    public void ShowShop()
    {
        gameObject.SetActive(true);
    }

    public void CloseShop()
    {
        gameObject.SetActive(false);
    }
}
