﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEditor;
using UnityEngine.UI;

public class SoundManаger : MonoBehaviour
{
    [SerializeField] public MusicController Music;
    public static SoundManаger SMSingleton;
    private AudioSource As;
    public AudioSource Bg;
    public float BgVolume = 1f;
    public float ЕffectVolume = 1f;

    //public Slider BgSleder;
    //public Slider EffectSlider;

    public void Awake()
    {
        if (!SMSingleton)
        {
            DontDestroyOnLoad(gameObject);
            SMSingleton = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        As = GetComponent<AudioSource>();
    }

    public void JumpMusic()
    {
        if(GameManager.GMSingleton.music == true)
        As.PlayOneShot(Music.Jump);
    }
    public void BonusMusic()
    {
        if (GameManager.GMSingleton.music == true)
            As.PlayOneShot(Music.Bonus);
    }
    public void WinMusic()
    {
        if (GameManager.GMSingleton.music == true)
            As.PlayOneShot(Music.Win);
    }
    public void DeadMusic()
    {
        if (GameManager.GMSingleton.music == true)
            As.PlayOneShot(Music.Dead);
    }

    public void BGNew()
    {
        GameObject currentGm;
        currentGm = GameObject.FindGameObjectWithTag("Bg");
        Bg = currentGm.GetComponent<AudioSource>();
        if (GameManager.GMSingleton.music == false)
        {
            Bg.mute = true;
        }
        else
        {
            Bg.mute = false;
        }
    }

    public void NewSlider(Slider SliderBg, Slider SliderEffect)
    {
        SliderBg.value = BgVolume;
        SliderEffect.value = ЕffectVolume;

        SliderBg.onValueChanged.AddListener(BgVolumeSlider);
        SliderEffect.onValueChanged.AddListener(ЕffectVolumeSlider);
    }

    public void BgVolumeSlider(float vol)
    {
        BgVolume = vol;
        Bg.volume = BgVolume;
    }
    public void ЕffectVolumeSlider(float vol)
    {
        ЕffectVolume = vol;
        As.volume = ЕffectVolume;
    }

}
[Serializable] 
public struct MusicController
{
    public AudioClip Jump;
    public AudioClip Bonus;
    public AudioClip Win;
    public AudioClip Dead;
}
