﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpriteSettings", menuName = "SpriteSettings", order = 51)]
public class SpriteSettings : ScriptableObject
{
    public List<Sprite> sprite;
}
