﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class StartPanel : MonoBehaviour
{
    public GameObject _popOnExitApp;
    public GameObject Chapters;

    public GameObject First_Chapter;
    public GameObject Second_Chapter;
    public GameObject Third_Chapter;


    public Button _quitAccept;
    public Button _quitDeny;

    public Button Vibration;
    public Sprite VibrationOn;
    public Sprite VibrationOff;

    public Button Music;
    public Sprite MusicOn;
    public Sprite MusicOff;


    public Slider SliderBg;
    public Slider SliderEffect;
    private void Awake()
    {
        
    }
    private void Start()
    {

        _quitAccept.onClick.AddListener(QuitAccept);
        _quitDeny.onClick.AddListener(QuitDeny);
        SoundManаger.SMSingleton.NewSlider(SliderBg, SliderEffect);

        if (GameManager.GMSingleton.vibration == false)
        {
            Vibration.image.sprite = VibrationOff;
        }
        else
        {
            Vibration.image.sprite = VibrationOn;
        }
        if (GameManager.GMSingleton.music == false)
        {
            Music.image.sprite = MusicOff;
        }
        else
        {
            Music.image.sprite = MusicOn;
        }
        SoundManаger.SMSingleton.BGNew();
        
        
    }

    public void Play()
    {
        //SceneManager.LoadScene(GameManager.GMSingleton.indexlevel);
        Chapters.SetActive(true);

    }
    public void FirstChapter()
    {
        First_Chapter.SetActive(true);
    }
    public void SecondChapter()
    {
        Second_Chapter.SetActive(true);
    }
    public void ThirdChapter()
    {
        Third_Chapter.SetActive(true);
    }
    public void CurrentLevel(int indexLevel)
    {
        SceneManager.LoadScene(indexLevel);
    }
    public void BackBotton()
    {
        Chapters.SetActive(false);
    }
    public void BackBotton1()
    {
        First_Chapter.SetActive(false);
    }
    public void BackBotton2()
    {
        Second_Chapter.SetActive(false);
    }
    public void BackBotton3()
    {
        Third_Chapter.SetActive(false);
    }

    public void QuitAccept()
    {
        Application.Quit();
    }

    public void PopOnExitApp()
    {
        _popOnExitApp.SetActive(true);
    }

    public void QuitDeny()
    {
        _popOnExitApp.SetActive(false);
    }

    public void GetMoney()
    {
        PlayerPrefs.SetInt("Money", 10000);
    }

    public void ClearAllPrfers()
    {
        PlayerPrefs.DeleteAll();
    }

    public void VibrationOnOff() 
    {
        if (GameManager.GMSingleton.vibration == true)
        {
            Vibration.image.sprite = VibrationOff;
            GameManager.GMSingleton.vibration = false;
        }
        else
        {
            Vibration.image.sprite = VibrationOn;
            GameManager.GMSingleton.vibration = true;
        }
    }
    public void MusicOnOff()
    {
        if (GameManager.GMSingleton.music == true)
        {
            Music.image.sprite = MusicOff;
            GameManager.GMSingleton.music = false;
        }
        else
        {
            Music.image.sprite = MusicOn;
            GameManager.GMSingleton.music = true;
        }
        SoundManаger.SMSingleton.BGNew();
    }

    private bool On = false;
    public void Option(GameObject Panel)
    {
       
        if(On == true)
        {
            Panel.SetActive(false);
            On = false;
        }
        else
        {
            Panel.SetActive(true);
            On = true;
        }
        
    }
}
